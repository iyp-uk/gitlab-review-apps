# Gitlab review app

Simple flask app to demonstrate the Gitlab Review Apps.

## How to run the app manually

Build
```console
$ docker build -t gitlab-review-apps .
```

Run
```console
$ docker run -p 5000:5000 -e PORT=5000 gitlab-review-apps
```
