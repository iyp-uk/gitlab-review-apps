FROM python:3

# Expose is NOT supported by Heroku
# EXPOSE 5000

WORKDIR /usr/src/app

COPY . .
RUN pip install --no-cache-dir -r requirements.txt

# Run the image as a non-root user
RUN adduser --disabled-password --gecos "" myuser
USER myuser

# Run the app.  CMD is required to run on Heroku
# $PORT is set by Heroku
CMD gunicorn --bind 0.0.0.0:$PORT wsgi
